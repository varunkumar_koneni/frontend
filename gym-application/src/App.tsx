import React from 'react';
import logo from './logo.svg';
import './App.css';
import Login from './Components/Login';
import TraineeRegistration from './Components/TraineeRegistration';
import RouteLinks from './Components/RouteLinks';
import HomePage from './Components/HomePage';
import Header from './Components/Header';
import Footer from './Components/Footer';
import TraineeProfile from './Components/TraineeProfile';
import UpdateTraineeProfile from './Components/UpdateTraineeProfile';

const App: React.FC = () => {
  return (
    <div className="App">
      <RouteLinks/>
    </div>
  );
}

export default App;