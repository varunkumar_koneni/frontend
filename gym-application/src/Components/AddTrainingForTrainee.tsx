import React, { useState, ChangeEvent, FormEvent } from 'react';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import MenuItem from '@mui/material/MenuItem';
import { ThemeProvider, createTheme } from '@mui/material/styles';
import TraineeHeader from './TraineeHeader';
import Footer from './Footer';

const theme = createTheme();


interface TrainingFormData {
  traineeUsername: string;
  trainerUsername: string;
  trainingName: string;
  trainingDate: string;
  trainingType: string;
  trainingDuration: string;
}

interface TrainerDTO {
  firstName: string;
  lastName: string;
  specialization: string;
  username: string;
}

const AddTrainingForTrainee: React.FC = () => {
  const username = localStorage.getItem('username');

  const [trainingFormData, setTrainingFormData] = useState<TrainingFormData>({
    traineeUsername: username || '',
    trainerUsername: '',
    trainingName: '',
    trainingDate: '',
    trainingType: '',
    trainingDuration: '',
  });

  const [trainersList, setTrainersList] = useState<TrainerDTO[]>([]);

  const handleInputChange = async (
    event: ChangeEvent<HTMLInputElement | { name?: string; value: unknown }>
  ) => {
    const { name, value } = event.target;
    setTrainingFormData((prevData) => ({
      ...prevData,
      [name as string]: value,
    }));

    if (name === 'trainingType') {
      try {
        const response = await fetch(`http://localhost:8080/trainers/trainingType/${value}`);
        const data = await response.json();
        setTrainersList(data);
      } catch (error) {
        console.error('Error fetching trainers:', error);
      }
    }
  };

  const handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    try {
      const response = await fetch('http://localhost:8080/trainings/add', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(trainingFormData),
      });

      if (response.ok) {
        console.log('Training added successfully');
      } else {
        console.error('Training adding Failed');
      }
    } catch (error) {
      console.error('Error during adding Training:', error);
    }
    

    
  };

  return (
    <ThemeProvider theme={theme}>
    <TraineeHeader/>
    <Container component="main" maxWidth="xs" sx={{ marginTop: 8, display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
      <Typography component="h1" variant="h5" align="center">
        Add Trainings
      </Typography>
      <form onSubmit={handleSubmit}>
        <TextField
          fullWidth
          label="Training date"
          type="date"
          name="trainingDate"
          value={trainingFormData.trainingDate}
          onChange={handleInputChange}
          InputLabelProps={{
            shrink: true,
          }}
        />

        <TextField
          margin="normal"
          fullWidth
          label="Training name"
          name="trainingName"
          value={trainingFormData.trainingName}
          onChange={handleInputChange}
          required
        />

        <TextField
          margin="normal"
          fullWidth
          select
          label="Training type"
          name="trainingType"
          value={trainingFormData.trainingType}
          onChange={handleInputChange}
          required
        >
          <MenuItem value="fitness">Fitness</MenuItem>
          <MenuItem value="yoga">Yoga</MenuItem>
          <MenuItem value="zumba">Zumba</MenuItem>
          <MenuItem value="stretching">Stretching</MenuItem>
          <MenuItem value="resistance">Resistance</MenuItem>
        </TextField>

        <TextField
          margin="normal"
          fullWidth
          select
          label="Trainer's name"
          name="trainerUsername"
          value={trainingFormData.trainerUsername}
          onChange={handleInputChange}
          required
        >
          {trainersList.map((trainer, index) => (
            <MenuItem key={index} value={trainer.username}>
              {`${trainer.firstName} ${trainer.lastName}`}
            </MenuItem>
          ))}
        </TextField>

        <TextField
          margin="normal"
          fullWidth
          label="Training duration"
          name="trainingDuration"
          value={trainingFormData.trainingDuration}
          onChange={handleInputChange}
          required
        />

          <Button type="submit" fullWidth variant="contained" sx={{ mt: 3, width: '50%' }}>
            Add Training
          </Button>
      </form>
    </Container>
    <Footer />
    </ThemeProvider>
  );
};

export default AddTrainingForTrainee;
