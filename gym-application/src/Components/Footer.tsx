import React from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Link from '@mui/material/Link';

const Footer = () => {
  return (
    <Box
      component="footer"
      sx={{
        backgroundColor: '#1976D2',
        color: 'white',
        padding: '1rem',
        textAlign: 'center',
        position: 'fixed',
        bottom: 0,
        width: '100%',
      }}
    >
      <Typography variant="body2" sx={{ mt: 2 }}>
        © 2023 VK Fitness Center. All rights reserved.
      </Typography>
      <Link href="#" color="inherit" sx={{ marginLeft: '0.5rem' }}>
          About us
      </Link>
      <Link href="#" color="inherit" sx={{ marginLeft: '0.5rem' }}>
        Contact us
      </Link>
      <Link href="#" color="inherit" sx={{ marginLeft: '0.5rem' }}>
        Terms of use
      </Link>
    </Box>
  );
};

export default Footer;
