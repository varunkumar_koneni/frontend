// LogoutButton.jsx
import React from 'react';
import { useNavigate } from 'react-router-dom';

const LogoutButton = () => {
  const navigate = useNavigate(); // useNavigate from react-router-dom

  const handleLogout = () => {
    // Clear the username from localStorage
    localStorage.removeItem('username');

    // Navigate to the login page
    navigate('/login');
  };

  return (
    <button onClick={handleLogout}>
      Logout
    </button>
  );
};

export default LogoutButton;
