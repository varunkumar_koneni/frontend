import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import AdbIcon from '@mui/icons-material/Adb';
import { useNavigate } from 'react-router-dom';

const TraineeHeader = () => {
  const navigate = useNavigate();
  const username = localStorage.getItem('username');

  const handleDeleteProfile = async () => {
    console.log('varun');
    console.log(username);
    try {
      const response = await fetch(`http://localhost:8080/trainees/${username}`, {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
        },
      });

      if (response.ok) {
        console.log('Profile deleted successfully.');
        // Navigate to the login page after successful deletion
        navigate('/login');
      } else {
        console.error('Failed to delete profile.');
      }
    } catch (error) {
      console.error('Error deleting profile:', error);
    }
  };
  const handleLogout = async () => {
    localStorage.removeItem('username');
    localStorage.removeItem('userType');

    navigate('/login');

  };


  const customPages = [
    { label: 'View Trainings', url: 'TrainingsLogForTrainee' },
    { label: 'Update Profile', url: 'UpdateTraineeProfile' },
    { label: 'Delete Profile', onClick: handleDeleteProfile },
    { label: 'Change Password', url: 'ChangePasswordForTrainee' },
    { label: 'Logout', onClick: handleLogout },

  ];

  return (
    <AppBar position="static">
      <Container maxWidth="xl">
        <Toolbar disableGutters>
          <AdbIcon sx={{ display: { xs: 'none', md: 'flex' }, mr: 1 }} />
          <Typography
            variant="h6"
            noWrap
            component="a"
            href="#app-bar-with-responsive-menu"
            sx={{
              mr: 2,
              display: { xs: 'none', md: 'flex' },
              fontFamily: 'monospace',
              fontWeight: 700,
              letterSpacing: '.3rem',
              color: 'inherit',
              textDecoration: 'none',
            }}
          >
            Gym App
          </Typography>

          <Box sx={{ flexGrow: 1 }} />

          <Box sx={{ display: { xs: 'none', md: 'flex' } }}>
            {customPages.map((page) => (
              <Button
                key={page.label}
                component="a"
                href={page.url}
                onClick={page.onClick}  // Use onClick directly
                sx={{ mx: 1, color: 'white', textDecoration: 'none' }}
              >
                {page.label}
              </Button>
            ))}
          </Box>
        </Toolbar>
      </Container>
    </AppBar>
  );
};

export default TraineeHeader;
