import React, { useEffect, useState } from 'react';
import { ThemeProvider, createTheme } from '@mui/material/styles';
import { useNavigate } from 'react-router-dom';
import { Table, TableContainer, TableHead, TableBody, TableRow, TableCell, Paper } from '@mui/material';


import {
  Container,
  Typography,
  TextField,
  RadioGroup,
  FormControlLabel,
  Radio,
  Grid,

} from '@mui/material';
import Footer from './Footer';
import TraineeHeader from './TraineeHeader';

const theme = createTheme();

interface Trainer {
  username: string;
  firstName: string;
  lastName: string;
  specialization: string;
}

interface Trainee {
  firstName: string;
  lastName: string;
  username: string;
  dateOfBirth: string;
  address: string;
  isActive: boolean;
  trainers: Trainer[]; 
}

const TraineeProfile = () => {
  const navigate = useNavigate();

  const [traineeData, setTraineeData] = useState<Trainee | null>(null);
  const username = localStorage.getItem('username');
  const userType = localStorage.getItem('userType');


  useEffect(() => {
    console.log(userType);
    fetch(`http://localhost:8080/trainees/${username}`)
      .then(response => {
        if (!response.ok) {
          if (response.status === 400) {
            navigate('/Login');
          } else {
            throw new Error(`HTTP error! Status: ${response.status}`);
          }
        }
        localStorage.setItem('userType', "trainee");
        return response.json();
      })
      .then(data => setTraineeData(data))
      .catch(error => {
        console.error('Error fetching trainer data:', error);
      });
  }, [username]);

  return (
    <ThemeProvider theme={theme}>
      <TraineeHeader />
      <Typography component="h1" variant="h5" align="center" sx={{ marginTop: 4 }}>
            Trainee Profile
          </Typography>
      <Container component="main" sx={{ marginTop: 4, display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
      <Paper elevation={5} sx={{ padding: 3, height: '50%', width: '80%', margin: '0 auto', marginTop: '10px' }}>

          <Grid container spacing={3}>
          <Grid  item xs={10} md={4}>
              {/* Trainer Details */}
              <TextField
                margin="normal"
                required
                fullWidth
                id="firstName"
                label="First Name"
                name="firstName"
                autoComplete="given-name"
                value={traineeData?.firstName || ''}
                InputLabelProps={{
                  shrink: true,
                }}
              />
              <TextField
                margin="normal"
                required
                fullWidth
                id="lastName"
                label="Last Name"
                name="lastName"
                autoComplete="family-name"
                value={traineeData?.lastName || ''}
                InputLabelProps={{
                  shrink: true,
                }}
              />
              <TextField
                margin="normal"
                required
                fullWidth
                label="Date of Birth"
                type="date"
                name="dateOfBirth"
                value={traineeData?.dateOfBirth || ''}
                InputLabelProps={{
                  shrink: true,
                }}
              />
              <TextField
                margin="normal"
                fullWidth
                id="address"
                label="Address"
                name="address"
                autoComplete="address"
                value={traineeData?.address || ''}
                InputLabelProps={{
                  shrink: true,
                }}
              />
              <RadioGroup
                row
                name="isActive"
                value={traineeData?.isActive ? 'true' : 'false'}
                sx={{ mt: 2 }}
              >
                <FormControlLabel value="true" control={<Radio />} label="Active" />
                <FormControlLabel value="false" control={<Radio />} label="Inactive" />
              </RadioGroup>
            </Grid>

            <Grid item xs={12} md={6} marginLeft={15}>
              <Typography component="h1" variant="h5" align="center" sx={{ marginTop: 3, marginBottom:2 }}>
            List of Trainers
          </Typography>
          <TableContainer component={Paper} sx={{ overflowY: 'auto', maxHeight: '300px' }}>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>First Name</TableCell>
                    <TableCell>Last Name</TableCell>
                    <TableCell>Specialization</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {traineeData?.trainers?.map((trainer, index) => (
                    <TableRow key={index}>
                      <TableCell>{trainer.firstName}</TableCell>
                      <TableCell>{trainer.lastName}</TableCell>
                      <TableCell>{trainer.specialization}</TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
            </Grid>
          </Grid>
        </Paper>
      </Container>
      <Footer />
    </ThemeProvider>
  );
};

export default TraineeProfile;
