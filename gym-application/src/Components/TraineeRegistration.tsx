// TraineeRegistration.jsx
import React, { useState } from 'react';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import { ThemeProvider, createTheme } from '@mui/material/styles';
import Header from './Header';
import Footer from './Footer';
import { useNavigate } from 'react-router-dom';

interface UserCredentials {
  username: string;
  password: string;
}

const theme = createTheme();

const TraineeRegistration = () => {
  const navigate = useNavigate();
  const [registrationSuccess, setRegistrationSuccess] = useState(false);
  const [credentials, setCredentials] = useState<UserCredentials | undefined>();

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    const formData = new FormData(event.currentTarget);
    const traineeData = {
      firstName: formData.get('firstName'),
      lastName: formData.get('lastName'),
      email: formData.get('email'),
      dateOfBirth: formData.get('dob'),
      address: formData.get('address'),
    };

    try {
      const response = await fetch('http://localhost:8080/trainees/register', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(traineeData),
      });

      if (response.ok) {
        const credentialsResponse: UserCredentials = await response.json();
        setCredentials(credentialsResponse);
        
        setRegistrationSuccess(true);
      } else {
        console.error('Registration failed');
      }
    } catch (error) {
      console.error('Error during registration:', error);
    }
  };

  const handleButtonClick = () => {
    if (credentials) {
      localStorage.setItem('username', credentials.username);
      navigate('/TraineeProfile'); 
    }
  };

  return (
    <ThemeProvider theme={theme}>
      <Header />
      <Container component="main" maxWidth="xs" sx={{ marginTop: 8, display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
        <Typography component="h1" variant="h5">
          {registrationSuccess ? 'Registration Successful' : 'Trainee Registration'}
        </Typography>
        
        {registrationSuccess ? (
          <>
            <TextField
              margin="normal"
              fullWidth
              id="username"
              label="Username"
              name="username"
              value={credentials?.username || ''}
              InputProps={{
                readOnly: true,
              }}
            />
            <TextField
              margin="normal"
              fullWidth
              id="password"
              label="Password"
              name="password"
              value={credentials?.password || ''}
              InputProps={{
                readOnly: true,
              }}
            />
            

            <Button onClick={handleButtonClick} fullWidth variant="contained" sx={{ mt: 3, width: '50%' }}>
              Next
            </Button>
          </>
        ) : (
          <form onSubmit={handleSubmit}>
            <TextField
              margin="normal"
              required
              fullWidth
              id="firstName"
              label="First Name"
              name="firstName"
              autoComplete="given-name"
            />
            <TextField
              margin="normal"
              required
              fullWidth
              id="lastName"
              label="Last Name"
              name="lastName"
              autoComplete="family-name"
            />
            <TextField
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email"
              name="email"
              type="email"
              autoComplete="email"
            />
            <TextField
              margin="normal"
              required
              fullWidth
              id="dob"
              label="Date of Birth"
              name="dob"
              autoComplete="bday"
              type="date"
              InputLabelProps={{
                shrink: true,
              }}
            />
            <TextField
              margin="normal"
              fullWidth
              id="address"
              label="Address"
              name="address"
              autoComplete="address"
            />
            <Button type="submit" fullWidth variant="contained" sx={{ mt: 3, width: '50%' }}>
              Register
            </Button>
          </form>
        )}
      </Container>
      <Footer />
    </ThemeProvider>
  );
};

export default TraineeRegistration;
