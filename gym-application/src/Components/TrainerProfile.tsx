import React, { useEffect, useState } from 'react';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import TextField from '@mui/material/TextField';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import Button from '@mui/material/Button';
import { ThemeProvider, createTheme } from '@mui/material/styles';
import TrainerHeader from './TrainerHeader';
import Footer from './Footer';
import { useNavigate } from 'react-router-dom';
import { Avatar, FormControlLabel, IconButton, Paper, Radio, RadioGroup } from '@mui/material';


const theme = createTheme();


interface Trainer {
  firstName: string;
  lastName: string;
  specialization: string;
  isActive: boolean;
  trainees: Trainee[];
}

interface Trainee {
  firstName: string;
  lastName: string;
}

const TrainerProfile = () => {
  const navigate = useNavigate();

  const [trainerData, setTrainerData] = useState<Trainer | null>(null);
  const username = localStorage.getItem('username');

  useEffect(() => {
    fetch(`http://localhost:8080/trainers/${username}`)
      .then(response => {
        localStorage.setItem('userType', "trainer");
        if (!response.ok) {
          if (response.status === 400) {
            console.error('Bad request. Redirecting to login page.');
            navigate('/Login');
          } else {
            throw new Error(`HTTP error! Status: ${response.status}`);
          }
        }
        localStorage.setItem('userType', "trainee");
        return response.json();
      })
      .then(data => setTrainerData(data))
      .catch(error => {
        console.error('Error fetching trainer data:', error);
        // Handle other error cases or redirect if needed
      });
  }, [username]);

  



  return (
    <ThemeProvider theme={theme}>
    <TrainerHeader />

    <Typography component="h1" variant="h5" align="center" sx={{ marginTop: 4 }}>
            Trainer Profile
      </Typography>
    <Container component="main" maxWidth="xs" sx={{ marginTop: 4, display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
      <Paper elevation={3} sx={{ padding: 3, width: '85%', flexDirection: 'column', alignItems: 'center' }}>
      <TextField
                margin="normal"
                required
                fullWidth
                id="firstName"
                label="First Name"
                name="firstName"
                autoComplete="given-name"
                value={trainerData?.firstName || ''}
                InputLabelProps={{
                  shrink: true,
                }}
              />
              <TextField
                margin="normal"
                required
                fullWidth
                id="lastName"
                label="Last Name"
                name="lastName"
                autoComplete="family-name"
                value={trainerData?.lastName || ''}
                InputLabelProps={{
                  shrink: true,
                }}
              />
              <TextField
                margin="normal"
                required
                fullWidth
                id="Username"
                label="Username"
                name="Username"

                value={username}
                InputLabelProps={{
                  shrink: true,
                }}
              />

              <TextField
                margin="normal"
                required
                fullWidth
                id="specialization"
                label="specialization"
                name="specialization"

                value={trainerData?.specialization}
                InputLabelProps={{
                  shrink: true,
                }}
              />
              
              <RadioGroup
                row
                name="isActive"
                value={trainerData?.isActive ? 'true' : 'false'}
                sx={{ mt: 2, display: 'flex', alignItems: 'center', marginLeft:10 }}
                >
                <FormControlLabel value="true" control={<Radio />} label="Active" />
                <FormControlLabel value="false" control={<Radio />} label="Inactive" />
              </RadioGroup>
  
        <ListItem sx={{marginLeft: 5 }}>
          <Typography component="span" sx={{ marginRight: 3 }}>
              Trainers List  :
            </Typography>
          <Select
            displayEmpty
            inputProps={{ readOnly: true }}
            native={true}
            variant="standard"
          >
            <option value="">Trainees</option>
            {trainerData?.trainees?.map((trainee, index) => (
              <option key={index} value={`${trainee.firstName} ${trainee.lastName}`}>
                {`${index + 1}: ${trainee.firstName} ${trainee.lastName}`}
              </option>
            ))}
          </Select>
        </ListItem>
  
      </Paper>
    </Container>
    <Footer />
  </ThemeProvider>
  
  );
};

export default TrainerProfile;
