import React from 'react';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import Divider from '@mui/material/Divider';
import { ThemeProvider, createTheme } from '@mui/material/styles';

const theme = createTheme();

const TrainerProfile = () => {
  // Assume you have trainer data and trainees data
  const trainerData = {
    firstName: 'TrainerFirst',
    lastName: 'TrainerLast',
    username: 'trainer.username',
    specialization: 'Web Development',
    isActive: true,
    traineesList: [
      { firstName: 'Trainee1', lastName: 'Doe' },
      { firstName: 'Trainee2', lastName: 'Smith' },
    ],
  };

  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="xs">
        <Typography component="h1" variant="h5" align="center">
          Trainer Profile
        </Typography>
        <List>
          <ListItem>
            <ListItemText primary={`First Name: ${trainerData.firstName}`} />
          </ListItem>
          <ListItem>
            <ListItemText primary={`Last Name: ${trainerData.lastName}`} />
          </ListItem>
          <ListItem>
            <ListItemText primary={`Username: ${trainerData.username}`} />
          </ListItem>
          <ListItem>
            <ListItemText primary={`Specialization: ${trainerData.specialization}`} />
          </ListItem>
          <ListItem>
            <ListItemText primary={`Is Active: ${trainerData.isActive ? 'Yes' : 'No'}`} />
          </ListItem>
          <Divider />
          <ListItem>
            <ListItemText primary="Trainees List" />
          </ListItem>
          {trainerData.traineesList.map((trainee, index) => (
            <ListItem key={index}>
              <ListItemText
                primary={`Trainee ${index + 1}: ${trainee.firstName} ${trainee.lastName}`}
              />
            </ListItem>
          ))}
        </List>
      </Container>
    </ThemeProvider>
  );
};

export default TrainerProfile;
